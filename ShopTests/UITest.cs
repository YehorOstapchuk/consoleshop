using Microsoft.VisualStudio.TestTools.UnitTesting;
using shopEPAM;
using System.Linq;
using System;

namespace ShopTests
{
    [TestClass]
    public class UITest
    {

        [TestCleanup]
        public void Clean()
        {
            Setup.DbReset();
        }

        [TestMethod]
        public void NewUserRegistrationTest()
        {
            GuestArgs args = new GuestArgs();
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia322";
            args.Password = "p322";
            args.PhoneNumber = "+7777777";
            Guest.Register(args);
            int amount = ShopDbMock.Users.Where((a => (a.NickName == args.NickName) && (a.Password == args.Password))).ToArray().Length;
            Assert.AreEqual(1, amount);
        }

        [TestMethod]
        public void SeveralUsersRegestrationTest()
        {
            GuestArgs args = new GuestArgs();
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia322";
            args.Password = "p322";
            args.PhoneNumber = "+7777777";
            GuestArgs args2 = new GuestArgs();
            args2.FirstName = "Petia";
            args2.SecondName = "Ivanov";
            args2.NickName = "Petia228";
            args2.Password = "p322";
            args2.PhoneNumber = "+88888888";
            Guest.Register(args);
            Guest.Register(args2);
            int amount = ShopDbMock.Users.Count();
            Assert.AreEqual(2, amount);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SameUserRegistrationTest()
        {
            GuestArgs args = new GuestArgs();
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia322";
            args.Password = "p322";
            args.PhoneNumber = "+7777777";
            Guest.Register(args);
            Guest.Register(args);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void LoginFailTest()
        {
            GuestArgs args = new GuestArgs();
            args.NickName = "edfh";
            args.Password = "kdf";
            Guest.LogIn(args);
        }

        [TestMethod]
        public void SuccessLogInUserTest()
        {
            GuestArgs args = new GuestArgs();
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia322";
            args.Password = "p322";
            args.PhoneNumber = "+7777777";
            Guest.Register(args);
            Guest.LogIn(args);
            Assert.IsNotNull(RegisteredUser.user);
        }

        [TestMethod]
        public void SuccesLogInAdminTest()
        {
            GuestArgs args = new GuestArgs();
            User user = User.Create("Petia", "Petrov", "Petia228", "228", null, UserType.Administrator);
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia228";
            args.Password = "228";
            args.PhoneNumber = "+7777777";
            ShopDbMock.AddUser(user);
            Guest.LogIn(args);
            Assert.IsNotNull(Admin.user);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void WrongPasswordLogInAdminTest()
        {
            GuestArgs args = new GuestArgs();
            User user = User.Create("Petia", "Petrov", "Petia228", "228", null, UserType.Administrator);
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia228";
            args.Password = "2289999";
            args.PhoneNumber = "+7777777";
            ShopDbMock.AddUser(user);
            Guest.LogIn(args);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void WrongLogInUserTest()
        {
            GuestArgs args = new GuestArgs();
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia322";
            args.Password = "p322";
            args.PhoneNumber = "+7777777";
            Guest.Register(args);
            args.Password = "fijg";
            Guest.LogIn(args);
        }

        [TestMethod]
        public void SuccessOrderCreationTest()
        {
            Good good = Good.Create("Iphone", 100, "Phone", "none");
            ShopDbMock.AddGood(good);
            GuestArgs args = new GuestArgs();
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia322";
            args.Password = "p322";
            args.PhoneNumber = "+7777777";
            Guest.Register(args);
            Guest.LogIn(args);
            RegisteredUserArgs args2 = new RegisteredUserArgs();
            args2.GoodId = 0;
            RegisteredUser.CreateOrder(args2);
            int amount = ShopDbMock.Orders.Count();
            Assert.AreEqual(1, amount);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void WrongOrderCreationTest()
        {
            Good good = Good.Create("Iphone", 100, "Phone", "none");
            ShopDbMock.AddGood(good);
            GuestArgs args = new GuestArgs();
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia322";
            args.Password = "p322";
            args.PhoneNumber = "+7777777";
            Guest.Register(args);
            Guest.LogIn(args);
            RegisteredUserArgs args2 = new RegisteredUserArgs();
            args2.GoodId = 1;
            RegisteredUser.CreateOrder(args2);
        }

        [TestMethod]
        public void ChangeOrderStatusToCompletedTest()
        {
            Good good = Good.Create("Iphone", 100, "Phone", "none");
            ShopDbMock.AddGood(good);
            GuestArgs args = new GuestArgs();
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia322";
            args.Password = "p322";
            args.PhoneNumber = "+7777777";
            Guest.Register(args);
            Guest.LogIn(args);
            RegisteredUserArgs args2 = new RegisteredUserArgs();
            args2.GoodId = 0;
            RegisteredUser.CreateOrder(args2);
            args2.OrderId = 0;
            args2.Confirm = true;
            RegisteredUser.ChangeOrderStatus(args2);
            int amount = ShopDbMock.Orders.Where(a => a.Status == OrderStatus.Completed).ToArray().Length;
            Assert.AreEqual(1, amount);
        }


        [TestMethod]
        public void ChangeOrderStatusToDeniedTest()
        {
            Good good = Good.Create("Iphone", 100, "Phone", "none");
            ShopDbMock.AddGood(good);
            GuestArgs args = new GuestArgs();
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia322";
            args.Password = "p322";
            args.PhoneNumber = "+7777777";
            Guest.Register(args);
            Guest.LogIn(args);
            RegisteredUserArgs args2 = new RegisteredUserArgs();
            args2.GoodId = 0;
            RegisteredUser.CreateOrder(args2);
            args2.OrderId = 0;
            args2.Confirm = false;
            RegisteredUser.ChangeOrderStatus(args2);
            int amount = ShopDbMock.Orders.Where(a => a.Status == OrderStatus.Denied).ToArray().Length;
            Assert.AreEqual(1, amount);
        }


        [TestMethod]
        public void ChangeUProfileNickNameTest()
        {
            GuestArgs args = new GuestArgs();
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia322";
            args.Password = "p322";
            args.PhoneNumber = "+7777777";
            Guest.Register(args);
            Guest.LogIn(args);
            RegisteredUserArgs args2 = new RegisteredUserArgs();
            args2.NickName = "Vasya";
            RegisteredUser.ChangeProfile(args2);
            int amount = ShopDbMock.Users.Where(a => a.NickName == args2.NickName).ToArray().Length;
            Assert.AreEqual(1, amount);
        }

        [TestMethod]
        public void ChangeUProfilePasswordTest()
        {
            GuestArgs args = new GuestArgs();
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia322";
            args.Password = "p322";
            args.PhoneNumber = "+7777777";
            Guest.Register(args);
            Guest.LogIn(args);
            RegisteredUserArgs args2 = new RegisteredUserArgs();
            args2.Password = "Vasya";
            RegisteredUser.ChangeProfile(args2);
            int amount = ShopDbMock.Users.Where(a => a.Password == args2.Password).ToArray().Length;
            Assert.AreEqual(1, amount);
        }

        [TestMethod]
        public void LogOutTest()
        {
            GuestArgs args = new GuestArgs();
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia322";
            args.Password = "p322";
            args.PhoneNumber = "+7777777";
            Guest.Register(args);
            Guest.LogIn(args);
            RegisteredUser.LogOut(new RegisteredUserArgs());
            Assert.IsNull(RegisteredUser.user);
        }

        [TestMethod]
        public void AdminAddGoodSuccessTest()
        {
            GuestArgs args = new GuestArgs();
            User user = User.Create("Petia", "Petrov", "Petia228", "228", null, UserType.Administrator);
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia228";
            args.Password = "228";
            args.PhoneNumber = "+7777777";
            ShopDbMock.AddUser(user);
            Guest.LogIn(args);
            AdminArgs args2 = new AdminArgs();
            args2.GoodName = "Iphone";
            args2.GoodPrice = 100;
            args2.GoodDescription = "sdfds";
            args2.GoodCategory = "dkjfd";
            Admin.AddGood(args2);
            int amount = ShopDbMock.Goods.Count();
            Assert.AreEqual(1, amount);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void AdminAddGoodWrongTest()
        {
            GuestArgs args = new GuestArgs();
            User user = User.Create("Petia", "Petrov", "Petia228", "228", null, UserType.Administrator);
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia228";
            args.Password = "228";
            args.PhoneNumber = "+7777777";
            ShopDbMock.AddUser(user);
            Guest.LogIn(args);
            AdminArgs args2 = new AdminArgs();
            args2.GoodName = "Iphone";
            args2.GoodPrice = 100;
            args2.GoodDescription = "sdfds";
            args2.GoodCategory = "dkjfd";
            Admin.AddGood(args2);
            Admin.AddGood(args2);
        }

        //[TestMethod]
        //public void AdminChangeOrderStatus()
        //{
            
            
        //    GuestArgs args3 = new GuestArgs();
        //    User user = User.Create("Petia", "Petrov", "Petia228", "228", null, UserType.Administrator);
        //    Good good = Good.Create("Iphone", 100, "Phone", "none");
        //    Order order = Order.Create(user, good);
        //    ShopDbMock.AddOrder(order);
        //    args3.FirstName = "Petia";
        //    args3.SecondName = "Petrov";
        //    args3.NickName = "Petia228";
        //    args3.Password = "228";
        //    args3.PhoneNumber = "+7777777";
        //    ShopDbMock.AddUser(user);
        //    Guest.LogIn(args3);
        //    AdminArgs args4= new AdminArgs();
        //    args4.Order = order;
        //    args4.OrderStatus = OrderStatus.DeniedByAdmin;
        //    Admin.ChangeOrdersStatus(args4);
        //    int amount = ShopDbMock.Orders.Where(a => a.Status == OrderStatus.DeniedByAdmin).Count();
        //    Assert.AreEqual(1, amount);
        //}

        [TestMethod]
        public void ChangeUserProfileByAdminTest()
        {
            GuestArgs args = new GuestArgs();
            User user = User.Create("Petia", "Petrov", "Petia228", "228", null, UserType.Administrator);
            User user2 = User.Create("Petr", "Petrov", "SuperPeyia", "p2000", null, UserType.Registered);
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia228";
            args.Password = "228";
            args.PhoneNumber = "+7777777";
            ShopDbMock.AddUser(user);
            ShopDbMock.AddUser(user2);
            Guest.LogIn(args);
            GuestArgs args2 = new GuestArgs();
            args2.SecondName = "Sidorov";
            Tools.ChangeUser(args2, user2);
            int amount = ShopDbMock.Users.Where(a => a.SecondName == args2.SecondName).Count();
            Assert.AreEqual(1, amount);
        }

        [TestMethod]
        public void ChangeGoodByAdminTest()
        {
            GuestArgs args = new GuestArgs();
            User user = User.Create("Petia", "Petrov", "Petia228", "228", null, UserType.Administrator);
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia228";
            args.Password = "228";
            args.PhoneNumber = "+7777777";
            ShopDbMock.AddUser(user);
            Guest.LogIn(args);
            Good good = Good.Create("Iphone", 100, "Phone", "none");
            ShopDbMock.AddGood(good);
            AdminArgs args2 = new AdminArgs();
            args2.GoodName = "Samsung";
            Tools.ChangeGood(args2,good);
            int amount = ShopDbMock.Goods.Where(a => a.Name == args2.GoodName).Count();
            Assert.AreEqual(1, amount);
        }


        [TestMethod]
        public void MakeUserAdminTest()
        {
            GuestArgs args = new GuestArgs();
            User user = User.Create("Petia", "Petrov", "Petia228", "228", null, UserType.Administrator);
            User user2 = User.Create("Petr", "Petrov", "SuperPeyia", "p2000", null, UserType.Registered);
            args.FirstName = "Petia";
            args.SecondName = "Petrov";
            args.NickName = "Petia228";
            args.Password = "228";
            args.PhoneNumber = "+7777777";
            ShopDbMock.AddUser(user);
            ShopDbMock.AddUser(user2);
            Guest.LogIn(args);
            AdminArgs args2 = new AdminArgs();
            args2.User = user2;
            Admin.MakeAdmin(args2);
            int amount = ShopDbMock.Users.Where(a => a.Status == UserType.Administrator).Count();
            Assert.AreEqual(2, amount);
        }
        
    }
}
